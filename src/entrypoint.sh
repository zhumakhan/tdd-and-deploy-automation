#!/bin/sh
go build main.go
case "$MODE" in
"TEST")
	go test
	;;
"DEV")
	./main
	;;
"PROD")
	./main
	;;
*)
	echo "NO MODE SPECIFIED"
	exit 1
	;;
esac

