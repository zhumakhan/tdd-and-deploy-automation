

package main

import (
	"flag"
	"log"
	"net/http"
)

func Sum(x float32, y float32)float32{
	return x+y
}
//UNEXPECTED UPDATE
func main() {
	port := flag.String("p", "8100", "port to serve on")
	directory := flag.String("d", ".", "the directory of static file to host")
	flag.Parse()

	http.Handle("/", http.FileServer(http.Dir(*directory)))

	log.Printf("Serving %s on HTTP port: %s\n", *directory, *port)
	log.Fatal(http.ListenAndServe(":"+*port, nil))
}